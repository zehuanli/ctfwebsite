package ctfServlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PictureAndMusic
 */
public class PictureAndMusicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String hint = "5dbd65c39443c46c8621afffdad6f098";
	private static final String flag = "192.168.54.102/one/two/978cdb236e5df22fccaeec1541aab03a.php";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PictureAndMusicServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String result = request.getParameter("result");
		response.getWriter().append("<html><body>");
		if (result == null || result.isEmpty()) {
			response.sendRedirect("e57fe170652883eced6228f575116df6.jsp");
		} else if (result.equals(hint)) {
			response.getWriter().append("Door opened.<br/>You follow the FBI and step inside.<br/>One step, two step, <b>WATCH OUT!</b><br/>" + flag);
		} else {
			response.getWriter().append("<html><body><p>Result not match</p><a href=\"e57fe170652883eced6228f575116df6.jsp\">Back</a></body></html>");
		}
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
