package ctfServlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Servlet implementation class SimpleQuery
 */
public class SimpleQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String flag="e57fe170652883eced6228f575116df6.jsp";
	private static final String realHint="ff40627dd11ea49c1b25b6f6410af902";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SimpleQueryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String hint=request.getParameter("hint");
		if(hint!=null&&hint.equals(realHint)){
			response.getWriter().append("GRANT ALL PRIVILEGES on *.* to YOU@...\nNo! You just need the location, right?\n" + flag);
			return;
		}
		String username = request.getParameter("username");
		if (username == null || username.isEmpty()) {
			response.sendRedirect("e98615916762f413a1279ad7c6da847e.jsp");
		}
		String sql = "select username,comment from users where username='" + username + "'";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		response.getWriter().append("<html><body>");
		try {
			conn = utils.MysqlConnection.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				response.getWriter().append("<p>"+rs.getString(1)+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+rs.getString(2)+"</p>");
			}
		} catch (Exception ee) {
			response.getWriter().append("Error, but, silence is golden.");
		}
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
