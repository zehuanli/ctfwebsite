package ctfServlets;

import java.io.IOException;
import java.nio.Buffer;
import java.security.MessageDigest;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioFormat.Encoding;

/**
 * Servlet implementation class StringsServlet
 */
public class StringsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String flag = "e98615916762f413a1279ad7c6da847e.jsp";
	private static final String charList = "0123456789abcdef";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StringsServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String result = request.getParameter("result");
		String hint = getCurrentHint();
		if (result == null || result.isEmpty()) {
			response.sendRedirect("8bcf6629759bd278a5c6266bd9c054f8.jsp");
		} else if (result.equals(hint)) {
			response.getWriter().append("Result match!\nLocation:" + flag);
		} else {
			response.getWriter().append("<html><body><p>Result not match</p><a href=\"8bcf6629759bd278a5c6266bd9c054f8.jsp\">Back</a></body></html>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			Integer quantity = Integer.parseInt(request.getParameter("quantity"));
			String s = null;
			Random ran = new Random();
			String hint = getCurrentHint();
			int interval = ran.nextInt(3) + 2;
			for (int i = 0; i < quantity; i++) {
				s = "";
				for (char c : hint.toCharArray()) {
					s += charList.charAt(ran.nextInt(charList.length())) + "";
					s += c + "";
					for (int j = 0; j < interval-1; j++) {
						s += charList.charAt(ran.nextInt(charList.length())) + "";
					}
				}
				response.getWriter().append(s + "\n");
			}
		} catch (Exception ee) {
			response.sendRedirect("strings.jsp");
		}
	}

	private String getCurrentHint() {
		String hint = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmm");
		String seed = flag + simpleDateFormat.format(System.currentTimeMillis());
		seed = seed.substring(0, seed.length() - 1) + (seed.charAt(seed.length()-1)>='5'?"-":"|");
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] md5Bytes = md5.digest(seed.getBytes("utf-8"));
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < md5Bytes.length; offset++) {
				i = md5Bytes[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			hint = buf.toString().substring(0, 10);
		} catch (Exception ee) {
		}
		return hint;
	}
}
