package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnection {
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String MYSQL_URL = "jdbc:mysql://localhost:3306/ctfdb?"
			+ "user=ctfuser&password=ctfuser@ctfdb#";

	public static Connection getConnection() throws ClassNotFoundException,SQLException {
		Class.forName(MYSQL_DRIVER);
		return DriverManager.getConnection(MYSQL_URL);
	}
}
