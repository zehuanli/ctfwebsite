<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>First task</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="page-header">
		<h1>
			A String Generator? <small>Check the string and find
				something</small>
		</h1>
		<p>A string generator is found left in Lily’s computer and the FBI
			believes it contains the specific location of Lily.</p>
	</div>
	<img src="files/radar.jpg" height="130" width="130" />
	<form action="8bcf6629759bd278a5c6266bd9c054f8.ctf" method="post">
		<input type="hidden" name="quantity" value="1" /> <input
			type="submit" value="Generate strings" class="btn btn-primary" />
	</form>
	<br />
	<form action="8bcf6629759bd278a5c6266bd9c054f8.ctf" method="get">
		<input type="text" placeholder="Got something?" name="result" /> <input
			type="submit" value="Submit" />
	</form>
	<br />
</body>
</html>