<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Let's have some music</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="page-header">
		<h1>
			A Strange Picture <small>Why are they using QRcode?</small>
		</h1>
		<p>A picture appears on the screen. FBI thinks that the picture may hide the password to open the door. </p>
		<br/>
		<p>Luckily enough, you have brought a smart phone with you.</p>
	</div>
	<img src="files/qrcode.jpg" />
	<form action="e57fe170652883eced6228f575116df6.ctf" method="get">
		What is behind the QRcode: <input type="text" name="result" /> <input type="submit"
			value="Submit" />
	</form>
	<br />
</body>
</html>