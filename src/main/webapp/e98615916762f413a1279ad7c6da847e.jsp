<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search for the flag</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="page-header">
		<h1>
			Search the database <small>Is there anything valuable in the
				database?</small>
		</h1>
		<p>After FBI get the location they set out immediately to release
			the hostage.</p>
		<p>When they arrive the building that the hostage has been
			detained in they knock out the guard.</p>
		<br />
		<p>A query interface of a database is on the screen.</p>
	</div>
	<form action="e98615916762f413a1279ad7c6da847e.ctf" method="get">
		<div class="alert alert-info" role="alert">
			Check a user like Alice: <input type="text" name="username" /> <input
				type="submit" value="Submit" />
		</div>

	</form>
	<br /> Hint: Table
	<i>users</i> consists of
	<i>username</i>,
	<i>comment</i> and
	<i>password</i>.
	<br />
</body>
</html>