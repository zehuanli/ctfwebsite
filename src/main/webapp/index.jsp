<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome to CTF Website</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="page-header">
		<div class="alert alert-danger" role="alert">
			<h1>URGENT!</h1>
		</div>
		<p>Recently, FBI has intercepted one critical intelligence, that a
			group of people is planning a terrorist attack in the next 24 hours.</p>
		<br />
		<p>As a veteran with rich practical experience and computer
			skills, YOU are asked to work with FBI to stop the criminal
			activities and maintain the territorial integrity.</p>
		<br />
		<p>
			<a class="btn btn-primary btn-lg" href="main.ctf" role="button">Click
				here to start!</a>
		</p>
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>